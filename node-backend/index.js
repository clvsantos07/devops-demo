const express = require('express')
const app = express()
const port = 3000

const logger = (data) => {
  console.log("[LOG]: ", new Date(), data)
}

app.get('/info', (req, res) => {
  logger("/info")
  res.json({ language: 'js', message: "Hello, from Node v" + process.env.NODE_VERSION  });
})

app.get('/', (req, res) => {
  logger("/")
  res.send('Hello, from NodeJS!')
})

app.listen(port, () => {
  console.log(`Node Backend listening at http://localhost:${port}`)
})
