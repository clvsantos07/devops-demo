## DevOps Demo
This repository contains the codebase used in the tutorial **"Automating your Application Deployment with GitLab CI, Docker, and AWS"**. The links to the articles can be found below.

[**Automating your Application Deployment with GitLab CI, Docker, and AWS (Part 1/3)**](https://medium.com/@clvsantos07/automating-your-application-deployment-with-gitlab-ci-docker-and-aws-part-1-3-8be534b66459) 

[**Automating your Application Deployment with GitLab CI, Docker, and AWS (Part 2/3)**](https://medium.com/@clvsantos07/automating-your-application-deployment-with-gitlab-ci-docker-and-aws-part-2-3-4b679cd43e1)

[**Automating your Application Deployment with GitLab CI, Docker, and AWS (Part 3/3)**](https://medium.com/@clvsantos07/automating-your-application-deployment-with-gitlab-ci-docker-and-aws-part-3-3-5036e0539384)