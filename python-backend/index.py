import os
from flask import Flask, jsonify

app = Flask(__name__)

@app.route('/info')
def info():
    return jsonify(
                language="python", 
                version=os.getenv("PYTHON_VERSION"),
                message=f"Hello, from Python v{(os.getenv('PYTHON_VERSION'))}"
            )

@app.route('/')
def hello_world():
    return f"Hello, from Python!"

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=3001, debug=True)